/**
 * Insert text at cursor's position in textbox.
 *
 *  Gets current text box and inserts given text in place of selected text.
 */
function insertText(element, text) {
    let priorText = element.value.substring(0, element.selectionStart);
    let latterText = element.value.substring(element.selectionEnd, element.value.length);
    element.value = priorText + text + latterText;
    element.focus();
}

/**
 * Print auth code to console
 * @param authResult
 */
function offAcc(authResult) {
    console.log('OFF CODE: ' + authResult['code']);
}

/**
 * Sign user out from google and the app backend.
 */
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
    console.log('Signing out from backend...');
    var xhr = new XMLHttpRequest();
    xhr.open('POST', logOutUrl);
    xhr.onload = function () {
        if (xhr.status === 200) {
            console.log('Signed out');
            location.reload(true);
        }
        console.log(xhr.status);
    };
    xhr.send();
}

/**
 * Callback to handle user signing in with google - also authenticates through the backend.
 * @param googleUser
 */
function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    var xhr = new XMLHttpRequest();
    console.log("sending data to backend...")
    xhr.open('POST', signInUrl);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        if (xhr.status === 202) {
            console.log('Signed in as: ' + xhr.responseText);
            window.location.reload(true);
        }
        console.log(xhr.status);
    };
    xhr.send('id-token=' + id_token);
}

/**
 * Callback function to open question editor panel.
 */
function newQuestion() {
    document.getElementById("questionEditor").classList.remove('hidden');
    document.getElementById("newButton").classList.add('hidden');
}

/**
 * Callback function to close question editor panel.

 * @param {Event}  e    The event that is calling this function.
 */
function closeEditor(e) {
    e.preventDefault();
    e.stopPropagation();
    document.getElementById("questionEditor").classList.add('hidden');
    document.getElementById("newButton").classList.remove('hidden');
}

/**
 * Generates a functino to be used onload based on the User's login status.
 *
 * @param signedIn
 * @returns {Function}
 */
function generateInitFunction(signedIn) {
    return function() {
        console.log("Google Libraries LOaded");
        if(signedIn) {
            gapi.load('auth2', function () {
                function initGapi() {
                    gapi.auth2.init();
                    window.console.log("gapi loaded, auth2 initted");
                }

                setTimeout(initGapi, 10);
            });
        }
        else {
            console.log('Not Signed In');
        }
    }
}

window.onload = generateInitFunction(userSignedIn);

/**
 * Store the user's id token as a global variable.
 *
 * @param googleUser
 */
function setIdToken(googleUser) {
    window.idToken = googleUser.getAuthResponse().id_token;
}

/**
 * Retrieves list of questions as specified in url variable which will be passed to callback function.
 *
 * @param questionId        id of the question being processed
 * @param parseResponses    callback function that processes
 */
function retrieveResponses(questionId, parseResponses) {
    let idToken = window.idToken;
    let requestData = {
        "id-token": idToken,
        "question-id": questionId
    };
    var xhr = new XMLHttpRequest();
    xhr.open('POST', responsesUrl);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function () {
        console.log(xhr.readyState);
        if (xhr.status == 200) {
            var responses = JSON.parse(xhr.response);
            console.log(xhr.response);
            console.log(responses);
            parseResponses(responses);
        }
        console.log(xhr.status);
    };
    xhr.send(JSON.stringify(requestData));
}

/**
 * Callback function for file picker that uploads image to IMGur. Inserts picture into textarea at later point.
 *
 * @param filePicker
 * @param elementId
 * @returns {boolean}
 */
function uploadImage(filePicker, elementId) {
    let files = filePicker.files;
    let apiUrl = 'https://api.imgur.com/3/image';
    let apiKey = imgurKey;
    for (var i = 0; i < files.length; i++) {
        var form = new FormData();
        let file = files[i];
        let filename = files[i].name;
        form.append("image", file);
        if (file.size > 5120000) {
            console.log("Please select a smaller file");
            return false;
        }
        let xhr = new XMLHttpRequest();
        xhr.open('POST', apiUrl);
        xhr.setRequestHeader('Authorization', 'CLIENT-ID ' + apiKey);
        xhr.onload = function () {
            var link = JSON.parse(xhr.responseText).data.link;
            insertText(document.getElementById(elementId), '![' + filename + '](' + link + ')');
        };
        xhr.send(form);
    }
}

/**
 * Removes the given tag from the given response.
 *
 * @param tagElement
 * @param questionId
 * @param responseId
 */
function removeTag(tagElement, questionId, responseId) {
    let idToken = window.idToken;
    let requestData = {
        "id-token": idToken,
        "question-id": questionId,
        "response-id": responseId,
        "tag": tagElement.innerText
    };

    var xhr = new XMLHttpRequest();
    console.log("Getting questions from backend...");
    xhr.open('POST', removeTagUrl);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function () {
        console.log(xhr.readyState);
        if (xhr.status == 200) {
            tagElement.remove();
        }
        console.log(xhr.status);
    };
    xhr.send(JSON.stringify(requestData));
}

/**
 * Adds the given tag to the given response.
 *
 * @param questionId
 * @param responseId
 * @param tag
 * @param onsuccess
 */
function addTag(questionId, responseId, tag, onsuccess) {
    let idToken = window.idToken;
    let requestData = {
        "id-token": idToken,
        "question-id": questionId,
        "response-id": responseId,
        "tag": tag
    };

    var xhr = new XMLHttpRequest();
    console.log("Getting questions from backend...");
    xhr.open('POST', addTagsUrl);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function () {
        console.log(xhr.readyState);
        if (xhr.status == 200) {
            console.log("Added Tag!");
            if (onsuccess) {
                onsuccess();
            }
        }
        console.log(xhr.status);
    };
    xhr.send(JSON.stringify(requestData));
}
