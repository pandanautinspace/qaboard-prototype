from flask import (Flask, render_template, request, redirect, url_for, session,
                   send_from_directory, abort)
from google.oauth2.credentials import Credentials
import json
from timeit import default_timer as timer
import os
from helpers import backend as b, users, datastructures, gsheets, gapi_helpers, server_storage
import hashids

os.environ['OAUTHLIB_RELAX_TOKEN_SCOPE'] = '1'

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.secret_key = 'zira_NO_moO16180_339887_PHI'

IMGUR_CLIENT_ID = '9f74fb2ee16e426'
IMGUR_CLIENT_SECRET = '99e5e6b8aae1f8f5df7afee6e5839dd199786747'


def render_template_helper(template_name_or_list, **kwargs):
    """Wrapper for render_template function which preloads some values to have less redundancy."""
    if 'teacher' in session.keys():
        return render_template(template_name_or_list, user_name=session['user_name'],
                               user_email=session['user_email'],
                               teacher_obj=session['teacher'], imgur_id=IMGUR_CLIENT_ID,
                               imgur_secret=IMGUR_CLIENT_SECRET, **kwargs)
    elif 'user_name' in session.keys():
        storage = server_storage.SQLiteStorage()
        if storage.teacher_exists(session['user_id']):
            (teacher_google_id, teacher_email, teacher_credentials_json, teacher_sheet_id) = \
                storage.get_all_teacher_info_from_google_id(google_id=session['user_id'])
            teacher_credentials_dict = json.loads(teacher_credentials_json)
            session['teacher'] = users.Teacher(
                session['user_name'],
                session['user_email'],
                session['user_id'],
                Credentials(**teacher_credentials_dict),
                teacher_sheet_id
            ).json_string
            return render_template(template_name_or_list, user_name=session['user_name'],
                                   user_email=session['user_email'], imgur_id=IMGUR_CLIENT_ID,
                                   imgur_secret=IMGUR_CLIENT_SECRET, teacher_obj=session['teacher'], **kwargs)
        else:
            return render_template(template_name_or_list, user_name=session['user_name'],
                                   user_email=session['user_email'],
                                   teacher_obj=None, imgur_id=IMGUR_CLIENT_ID,
                                   imgur_secret=IMGUR_CLIENT_SECRET, **kwargs)
    else:
        return render_template(template_name_or_list, user_name=None,
                               user_email=None,
                               teacher_obj=None, imgur_id=IMGUR_CLIENT_ID,
                               imgur_secret=IMGUR_CLIENT_SECRET, **kwargs)


@app.route('/')
def main_page():
    return render_template_helper('frontpage.html')


@app.route('/q/', methods=['GET', 'POST'])
@app.route('/q/<string:shortcode>/', methods=['GET', 'POST'])
def question_page(shortcode=None):
    code = ''
    if shortcode:
            code = shortcode
    else:
        code = request.args.get('shortcode','')
    try:
        question = b.unauthorized_retrieve_question(code)
    except ValueError:
        try:
            hasher = hashids.Hashids(min_length=3)
            teacher_id = hasher.decode(code)[0]
            storage = server_storage.SQLiteStorage()
            teacher = storage.get_teacher_object_from_teacher_id(teacher_id)
            backend = b.GDriveBackend(teacher)
            question = backend.get_last_question()
        except:
            return abort(404)
    return render_template_helper('questionpage.html', question=question)

@app.route('/p/<string:shortcode>/')
def share_page(shortcode=None):
    if shortcode:
        code = shortcode
        try:
            question = b.unauthorized_retrieve_question(code)
        except ValueError:
            try:
                hasher = hashids.Hashids(min_length=3)
                teacher_id = hasher.decode(code)[0]
                storage = server_storage.SQLiteStorage()
                teacher = storage.get_teacher_object_from_teacher_id(teacher_id)
                backend = b.GDriveBackend(teacher)
                question = backend.get_last_question()
            except:
                return abort(404)
        return render_template_helper('sharepage.html', question=question)

@app.route('/respond/<string:shortcode>/', methods=['POST'])
def add_response(shortcode=None):
    response_body = request.form['response']
    response_id = b.Backend.generate_response_id()
    response_owner = session['user_name']
    response = datastructures.Response(response_id, response_body, response_owner)
    b.unauthorized_submit_response(shortcode=shortcode, response=response)
    return redirect(url_for('question_page', shortcode=shortcode))


@app.route('/add/', methods=['POST'])
def add_question():
    if 'user_name' in session.keys():
        storage = server_storage.SQLiteStorage()
        title = request.form['title']
        body = request.form['body']
        owner = session['user_name']
        google_id = session['user_id']
        teacher = users.Teacher.from_json(session['teacher'])
        id = b.Backend.generate_question_shortcode(title)
        question = datastructures.Question(id, title, body, owner)
        gdrive = b.GDriveBackend(teacher)
        gdrive.add_question(question)
        storage.add_question(id, google_id)
        storage.commit()
    return redirect(url_for('dashboard_page'))


@app.route('/dashboard/')
def dashboard_page():
    if 'teacher' in session.keys():
        teacher = users.Teacher.from_json(session['teacher'])
        gdrive = b.GDriveBackend(teacher)
        teacher_id = gdrive.storage.get_teacher_id(teacher.google_id)
        hasher = hashids.Hashids(min_length=3)
        hash_shortcode = hasher.encode(teacher_id)
        questions = gdrive.get_questionList()
        return render_template_helper('dashboard_page.html', questions=questions, live_code=hash_shortcode)
    return abort(403)


@app.route('/r/<string:question_id>/')
def response_page(question_id):
    if 'teacher' in session.keys():
        teacher = users.Teacher.from_json(session['teacher'])
        gdrive = b.GDriveBackend(teacher)
        responses = gdrive.get_responseList(question_id)
        return render_template_helper('response_page.html', responses=responses)
    return abort(403)


@app.route('/api/retrieve-questions/', methods=['POST'])
def api_get_questions():
    id_token = request.json['id-token']
    user = gapi_helpers.get_user_from_auth_token(id_token)
    teacher = server_storage.SQLiteStorage().get_teacher_object_from_teacher_google_id(user.google_id)
    gdrive = b.GDriveBackend(teacher)
    questions = gdrive.get_questionList()
    result = json.dumps(questions)
    return result, 200, {'ContentType': 'application/json'}


@app.route('/api/get-responses/', methods=['POST'])
def api_get_responses():
    print(request.json)
    id_token = request.json['id-token']
    user = gapi_helpers.get_user_from_auth_token(id_token)
    teacher = server_storage.SQLiteStorage().get_teacher_object_from_teacher_google_id(user.google_id)
    question_id = request.json['question-id']
    gdrive = b.GDriveBackend(teacher)
    responses = gdrive.get_responseList(question_id)
    responses_list = []
    for response in responses.responses:
        rendered_text = response.render()
        responses_list.append({
            'body': rendered_text,
            'id': response.id,
            'owner': response.owner,
            'tags': response.tags
        })
    result = json.dumps(responses_list)
    return result, 200, {'Content-Type': 'application/json'}


@app.route('/api/tag-response/', methods=['POST'])
def api_tag_response():
    id_token = request.json['id-token']
    user = gapi_helpers.get_user_from_auth_token(id_token)
    teacher = server_storage.SQLiteStorage().get_teacher_object_from_teacher_google_id(user.google_id)
    question_id = request.json['question-id']
    response_id = request.json['response-id']
    tag = request.json['tag']
    gdrive = b.GDriveBackend(teacher)
    gdrive.tag_response(question_id, response_id, tag)
    result = "Success!"
    return result, 200, {'Content-Type': 'application/json'}


@app.route('/api/tag-remove/', methods=['POST'])
def api_tag_remove():
    id_token = request.json['id-token']
    user = gapi_helpers.get_user_from_auth_token(id_token)
    teacher = server_storage.SQLiteStorage().get_teacher_object_from_teacher_google_id(user.google_id)
    question_id = request.json['question-id']
    response_id = request.json['response-id']
    tag = request.json['tag']
    gdrive = b.GDriveBackend(teacher)
    gdrive.remove_tag(question_id, response_id, tag)
    result = "Success!"
    return result, 200, {'Content-Type': 'application/json'}


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/auth/', methods=['POST'])
def token_auth():
    try:
        token = request.form['id-token']
        user = gapi_helpers.get_user_from_auth_token(token)
        session['user_id'] = user.google_id
        session['user_email'] = user.email
        session['user_name'] = user.name
        return json.dumps(dict(success=True)), 202, dict(ContentType='application/json')
    except ValueError:
        # Invalid token
        return abort(403)


@app.route('/permissions/')
def token_receive():
    if 'user_name' in session.keys():
        flow = gapi_helpers.gen_flow()
        authorization_url, state = flow.authorization_url(
            access_type='offline',
            include_granted_scopes='true',
            login_hint=session['user_email'],
            prompt='consent'
        )
        session['state'] = state
        return redirect(authorization_url)
    else:
        return abort(403)


@app.route('/oauth2callback/', methods=['GET'])
def oauth2_callback():
    authorization_response = request.url
    credentials = gapi_helpers.get_credentials_from_auth_response(authorization_response)
    session['credentials'] = gapi_helpers.dict_from_credentials(credentials)
    google_id = session['user_id']
    storage = server_storage.SQLiteStorage()
    if storage.teacher_exists(google_id):
        (teacher_google_id, teacher_email, teacher_auth_refresh_token, teacher_sheet_id) = \
            storage.get_all_teacher_info_from_google_id(google_id=google_id)
        session['teacher'] = users.Teacher(name=session['user_name'], email=session['user_email'],
                                           google_id=teacher_google_id, refresh_token=teacher_auth_refresh_token,
                                           sheet_id=teacher_sheet_id).json_string
    else:
        teacher_google_id = google_id
        service = gapi_helpers.get_service_from_credentials(credentials)
        teacher_sheet_id = gsheets.create_new_datasheet(service)
        session['teacher'] = users.Teacher(name=session['user_name'], email=session['user_email'],
                                           google_id=teacher_google_id, credentials=credentials,
                                           sheet_id=teacher_sheet_id).json_string
        storage.register_teacher(teacher_google_id=teacher_google_id,
                                 teacher_sheet_id=teacher_sheet_id,
                                 teacher_auth_json_credentials=json.dumps(
                                     gapi_helpers.dict_from_credentials(credentials)
                                 ),
                                 teacher_email=session['user_email'])
        storage.commit()
    return redirect(url_for('dashboard_page'))


@app.route('/logout/', methods=['POST', 'GET'])
def log_out():
    if 'user_id' in session.keys():
        session.pop('user_id')
        session.pop('user_email')
        session.pop('user_name')
        if 'credentials' in session.keys():
            session.pop('credentials')
        if 'teacher' in session.keys():
            session.pop('teacher')
        if 'state' in session.keys():
            session.pop('state')
        if request.method == 'GET':
            return redirect(url_for('main_page'))
        else:
            return json.dumps(dict(success=True)), 200, dict(ContentType='application/json')
    if request.method == 'GET':
        return redirect(url_for('main_page'))
    else:
        return json.dumps(dict(success=True)), 200, dict(ContentType='application/json')


@app.errorhandler(403)
def unauthorized(e):
    return render_template_helper('403.html'), 403


@app.errorhandler(404)
def page_not_found(e):
    return render_template_helper('404.html'), 404


@app.errorhandler(405)
def method_not_allowed(e):
    return 'Well that doesn\'t work, does it?', 405


@app.errorhandler(500)
def method_not_allowed(e):
    return 'Sorry, our server encountered an error!', 500


if __name__ == '__main__':
    app.run(debug=True, threaded=True)
