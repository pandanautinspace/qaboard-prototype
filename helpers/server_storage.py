from helpers import users
from google.oauth2.credentials import Credentials

import json
import os
import sqlite3


class SQLiteStorage:
    """SQLiteStorage class. Wraps many SQLite operations in order to facilitate persistent storage.
    TODO: Rewrite storage as an abstract class which SQLiteStorage implements"""
    _DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'qa.db')
    _TEACHER_INSERTION = "INSERT INTO teachers (teacher_google_id, teacher_email, " \
                         "teacher_auth_json_credentials, teacher_sheet_id) " \
                         "VALUES (?, ?, ?, ?)"
    _QUESTION_INSERTION = "INSERT INTO shortcodes (shortcode, owners_id) VALUES (?, ?)"
    _FIND_TEACHER_ID = "SELECT teacher_id FROM teachers WHERE teacher_email = ?"
    _DELETE_TEACHER_BY_ID = "DELETE FROM teachers WHERE teacher_id = ?"
    _DELETE_QUESTION_BY_SHORTCODE = "DELETE FROM shortcodes WHERE shortcode = ?"

    def __init__(self, location=_DEFAULT_PATH):
        self.location = location
        self._con = sqlite3.connect(location)
        self._con.execute('''CREATE TABLE IF NOT EXISTS teachers 
                        (teacher_id INTEGER PRIMARY KEY,
                        teacher_google_id TEXT UNIQUE NOT NULL,
                        teacher_email TEXT UNIQUE NOT NULL,
                        teacher_auth_json_credentials TEXT UNIQUE NOT NULL,
                        teacher_sheet_id TEXT NOT NULL);''')
        self._con.execute('''CREATE TABLE IF NOT EXISTS shortcodes
                        (question_id INTEGER PRIMARY KEY,
                        shortcode TEXT UNIQUE NOT NULL,
                        owners_id TEXT NOT NULL);''')

    def register_teacher(self, teacher_google_id, teacher_email, teacher_auth_json_credentials, teacher_sheet_id):
        """Register teacher's details in the database."""
        self._con.execute(self._TEACHER_INSERTION,
                          (teacher_google_id, teacher_email, teacher_auth_json_credentials, teacher_sheet_id))

    def drop_teacher(self, teacher_email):
        """Remove teacher from the database."""
        teacher_id = self._con.execute(self._FIND_TEACHER_ID, (teacher_email,)).fetchone()[0]
        self._con.execute(self._DELETE_TEACHER_BY_ID, (teacher_id,))

    def add_question(self, shortcode, teacher_id):
        """Add question's shortcode to be located in the database."""
        self._con.execute(self._QUESTION_INSERTION, (shortcode, teacher_id))

    def drop_question(self, shortcode):
        """Remove question's shortcode from database so it is no longer accessible."""
        self._con.execute(self._DELETE_QUESTION_BY_SHORTCODE, (shortcode,))

    def drop_questions_by_teacher(self, teacher_id):
        """Remove all questions owned by a teacher."""
        self._con.execute("DELETE FROM shortcodes WHERE owners_id = ?", (teacher_id,))

    def get_teacher_sheet_id(self, teacher_id):
        """Retrieve a teacher's sheet id by their id in the table."""
        cur = self._con.execute("SELECT teacher_sheet_id FROM teachers WHERE teacher_id = ?", (teacher_id,))
        return cur.fetchone()[0]

    def get_teacher_sheet_id_by_google_id(self, teacher_google_id):
        """Retrieve a teacher's sheet id by their google id in the table."""
        cur = self._con.execute("SELECT teacher_sheet_id FROM teachers WHERE teacher_google_id = ?",
                                (teacher_google_id,))
        return cur.fetchone()[0]

    def get_teacher_sheet_id_by_email(self, teacher_email):
        """Retrieve a teacher's sheet id by their email in the table."""
        cur = self._con.execute("SELECT teacher_sheet_id FROM teachers WHERE teacher_email = ?",
                                (teacher_email,))
        return cur.fetchone()[0]

    def get_teacher_from_question(self, question_shortcode):
        """Get a teacher's id in the teachers table from a question's shortcode."""
        cur = self._con.execute("SELECT owners_id FROM shortcodes WHERE shortcode = ?",
                                (question_shortcode,))
        return cur.fetchone()

    def teacher_exists(self, teacher_google_id):
        """Check if a teacher is in the table based on their google id."""
        cur = self._con.execute("SELECT teacher_id FROM teachers WHERE teacher_google_id = ?",
                                (teacher_google_id,))
        return True if cur.fetchone() else False

    def question_exists(self, shortcode):
        """Check if a question exists based on their shortcode."""
        cur = self._con.execute("SELECT shortcode FROM shortcodes WHERE shortcode = ?",
                                (shortcode,))
        return True if cur.fetchone() else False

    def get_teacher_credentials(self, teacher_google_id):
        """Get a teacher's Credentials object represented as a JSON string based on their google id."""
        cur = self._con.execute("SELECT teacher_auth_json_credentials FROM teachers WHERE teacher_email = ?",
                                (teacher_google_id,))
        return cur.fetchone()[0]

    def get_teacher_id(self, teacher_google_id):
        """Get a teacher's index in the teacher table based on their google id."""
        cur = self._con.execute("SELECT teacher_id FROM teachers WHERE teacher_google_id = ?", (teacher_google_id,))
        return cur.fetchone()[0]

    def get_all_teacher_info_from_google_id(self, google_id):
        """Get all information stored about a teacher in the table based on their google id."""
        cur = self._con.execute("SELECT teacher_google_id, teacher_email, teacher_auth_json_credentials, "
                                "teacher_sheet_id FROM teachers WHERE teacher_google_id = ?", (google_id,))
        return cur.fetchone()

    def get_teacher_object_from_teacher_id(self, teacher_id):
        """Construct a teacher object based on all of the information stored about a teacher in the table.
        Takes their index in the table."""
        (teacher_google_id, teacher_email, teacher_auth_credentials, teacher_sheet_id) = self._con.execute(
            "SELECT teacher_google_id, teacher_email, teacher_auth_json_credentials, "
            "teacher_sheet_id FROM teachers WHERE teacher_id = ?", (teacher_id,)).fetchone()
        return users.Teacher(name='', email=teacher_email, google_id=teacher_google_id,
                             credentials=Credentials(**json.loads(teacher_auth_credentials)),
                             sheet_id=teacher_sheet_id)

    def get_teacher_object_from_teacher_google_id(self, teacher_google_id):
        """Construct a teacher object based on all of the information stored about a teacher in the table.
        Takes their google id."""
        (teacher_google_id, teacher_email, teacher_auth_credentials, teacher_sheet_id) = self._con.execute(
            "SELECT teacher_google_id, teacher_email, teacher_auth_json_credentials, "
            "teacher_sheet_id FROM teachers WHERE teacher_google_id = ?", (teacher_google_id,)).fetchone()
        return users.Teacher(name='', email=teacher_email, google_id=teacher_google_id,
                             credentials=Credentials(**json.loads(teacher_auth_credentials)),
                             sheet_id=teacher_sheet_id)

    def commit(self):
        """Commit the changes to the SQLite database."""
        return self._con.commit()

    def close(self):
        """Close the connection to the SQLite database."""
        return self._con.close()

    def rollback(self):
        """Roll back all uncommitted changes to the SQLite databse."""
        return self._con.rollback()
