from helpers import users, datastructures, gsheets, gapi_helpers, server_storage
from google.oauth2.credentials import Credentials
from abc import ABCMeta, abstractmethod
from random import randint
from time import time_ns

import hashids
import pickle
import json


class Backend(metaclass=ABCMeta):
    """Abstract class that outlines how the question storage backend will work."""
    def __init__(self):
        self.data = datastructures.QAData()

    @abstractmethod
    def add_question(self, question):
        raise NotImplementedError("Class %s doesn't implement add_question()" % self.__class__.__name__)

    @abstractmethod
    def get_question(self, question_id):
        raise NotImplementedError("Class %s doesn't implement get_question()" % self.__class__.__name__)

    @abstractmethod
    def get_question_by_shortcode(self, shortcode):
        raise NotImplementedError("Class %s doesn't implement get_question_by_shortcode()" % self.__class__.__name__)

    @abstractmethod
    def get_responseList(self, question):
        raise NotImplementedError("Class %s doesn't implement get_responselist()" % self.__class__.__name__)

    @abstractmethod
    def get_questionList(self):
        raise NotImplementedError("Class %s doesn't implement get_questionlist()" % self.__class__.__name__)

    @abstractmethod
    def add_response(self, question_id, response):
        raise NotImplementedError("Class %s doesn't implement add_response()" % self.__class__.__name__)

    @abstractmethod
    def add_response_by_shortcode(self, shortcode, response):
        raise NotImplementedError("Class %s doesn't implement add_response_by_shortcode()" % self.__class__.__name__)

    @abstractmethod
    def tag_response(self, shortcode, response_id, tag):
        raise NotImplementedError("Class %s doesn't implement tag_response()" % self.__class__.__name__)

    @staticmethod
    def generate_question_shortcode(question_title):
        hash_id = hashids.Hashids(salt=question_title)
        number_to_hash = time_ns() % 100000 * 100 + randint(-95, 2005)
        shortcode = hash_id.encode(number_to_hash)
        return shortcode

    @staticmethod
    def generate_response_id():
        hash_id = hashids.Hashids()
        return hash_id.encode(time_ns() % 10000 + time_ns() // 100000000)

    @abstractmethod
    def store_data(self):
        raise NotImplementedError("Class %s doesn't implement store_data()" % self.__class__.__name__)


class PickleBackend(Backend):
    """Backend implementation storing all data in a QAData object, which is then pickled. Do not use in production.
    Wraps QAData object functions."""

    def __init__(self, location):
        self.location = location
        try:
            with open(location, 'rb') as store:
                self.data = pickle.load(store)
        except (FileNotFoundError, EOFError):
            super().__init__()

    def generate_data_structure(self) -> datastructures.QAData:
        return self.data

    def store_data(self):
        with open(self.location, 'wb') as store:
            pickle.dump(self.data, store)

    def add_question(self, question):
        self.data.add_question(question.title, question.body, question.owner)

    def get_question(self, question_id):
        self.data.get_question(question_id)

    def get_question_by_shortcode(self, shortcode):
        self.data.get_question(shortcode)

    def get_responseList(self, question):
        self.data.get_response_list(question.ID)

    def get_questionList(self, user):
        self.data.get_question_list()

    def add_response_by_shortcode(self, shortcode, response):
        pass

    def tag_response(self, shortcode, response_id, tag):
        pass

    def add_response(self, question_id, response):
        pass


class GDriveBackend(Backend):
    """Backend implementation that stores question data in Google Drive. Wraps functions in the other modules.
    Created with a teacher object, allows access to and manipulation a teacher's questions."""

    def __init__(self, teacher):
        self.storage = server_storage.SQLiteStorage()
        self.user = teacher
        self.credentials = teacher.credentials
        self.service = gapi_helpers.get_service_from_credentials(self.credentials)
        self.sheet_id = teacher.sheet_id

    @classmethod
    def from_auth_token(cls, auth_token):
        """Generate GDriveBackend object from a Google Auth token."""
        user = gapi_helpers.get_user_from_auth_token(auth_token)
        credentials = gapi_helpers.get_credentials_from_auth_response(auth_token)
        teacher = users.Teacher.from_user(user, credentials, None)
        return cls(teacher)

    @classmethod
    def from_google_id(cls, google_id, name, email):
        """Generate GDriveBackend object from Google Account Information"""
        storage = server_storage.SQLiteStorage()
        if not storage.teacher_exists(google_id):
            raise ValueError
        credentials = storage.get_teacher_credentials(google_id)
        sheet_id = storage.get_teacher_sheet_id_by_google_id(google_id)
        teacher = users.Teacher(name=name, email=email, sheet_id=sheet_id, google_id=google_id,
                                credentials=credentials)
        return cls(teacher)

    def add_question(self, question):
        """Add a question to the backend storage, in this case Google Sheets."""
        if not self.service:
            raise ValueError
        return gsheets.add_question(question=question, service=self.service, spreadsheet_id=self.sheet_id)

    def add_response(self, question_id, response):
        """Add a response to a question in the Teacher's Google Sheet, identifying the question with id."""
        gsheets.add_response_to_question(self.service, self.sheet_id, question_id, response)

    def add_response_by_shortcode(self, shortcode, response):
        """Add a response to a question in the Teacher's Google Sheet, identifying the question with shortcode."""
        gsheets.add_response_to_question(service=self.service, question_shortcode=shortcode,
                                         spreadsheet_id=self.sheet_id, response=response)

    def get_question(self, question_id):
        """Get a question based on its id in the teacher's google sheet."""
        return gsheets.get_question_by_id(self.service, spreadsheet_id=self.sheet_id, owner=self.user.name,
                                          question_id=question_id)

    def get_question_by_shortcode(self, shortcode):
        """Get a question based on its shortcode in the teacher's google sheet."""
        if not self.service:
            raise ValueError
        return gsheets.get_question(self.service, spreadsheet_id=self.sheet_id,
                                    owner=self.user.name,
                                    question_shortcode=shortcode)

    def get_last_question(self):
        """Get the last question in the teacher's google sheet."""
        return gsheets.get_last_question(self.service, self.sheet_id)

    def get_responseList(self, question_id):
        """Get a ResponseList object giving responses based on a question shortcode in the teacher's google sheet."""
        if not self.service:
            raise ValueError
        return gsheets.get_responseList(self.service, self.sheet_id, question_id)

    def get_questionList(self):
        """Get a QuestionList object from teacher's google sheet."""
        if not self.service:
            raise ValueError
        return gsheets.get_questionList(self.service, self.sheet_id)

    def tag_response(self, shortcode, response_id, tag):
        """Wrapper to add a tag to a response."""
        gsheets.add_tag(question_id=shortcode, response_id=response_id, tag=tag, service=self.service,
                        spreadsheet_id=self.sheet_id)

    def remove_tag(self, shortcode, response_id, tag):
        """Wrapper to remove a tag from a response."""
        gsheets.remove_tag(question_id=shortcode, response_id=response_id, tag=tag, service=self.service,
                           spreadsheet_id=self.sheet_id)

    def store_data(self):
        pass


def unauthorized_retrieve_question(shortcode):
    """Retrieve a question by its shortcode without a teacher object provided - retrieves credentials from storage."""
    storage = server_storage.SQLiteStorage()
    if not storage.question_exists(shortcode):
        raise ValueError
    teacher_id = storage.get_teacher_from_question(shortcode)[0]
    print(teacher_id)
    (teacher_google_id, teacher_email, teacher_auth_json_credentials, teacher_sheet_id) = \
        storage.get_all_teacher_info_from_google_id(teacher_id)
    teacher_auth_credentials_dict = json.loads(teacher_auth_json_credentials)
    credentials = Credentials(**teacher_auth_credentials_dict)
    teacher = users.Teacher(name='', email=teacher_email, google_id=teacher_google_id,
                            credentials=credentials, sheet_id=teacher_sheet_id)
    unauthorized_backend = GDriveBackend(teacher)
    try:
        question = unauthorized_backend.get_question_by_shortcode(shortcode)
    except Exception:
        raise ValueError
    return question


def unauthorized_submit_response(shortcode, response):
    """Submit a response to a question by its shortcode without a teacher object provided.
    Retrieves credentials from storage."""
    storage = server_storage.SQLiteStorage()
    teacher_id = storage.get_teacher_from_question(shortcode)[0]
    print(teacher_id)
    (teacher_google_id, teacher_email, teacher_auth_json_credentials, teacher_sheet_id) = \
        storage.get_all_teacher_info_from_google_id(teacher_id)
    teacher_auth_credentials_dict = json.loads(teacher_auth_json_credentials)
    credentials = Credentials(**teacher_auth_credentials_dict)
    teacher = users.Teacher(name='', email=teacher_email, google_id=teacher_google_id,
                            credentials=credentials, sheet_id=teacher_sheet_id)
    unauthorized_backend = GDriveBackend(teacher)
    unauthorized_backend.add_response_by_shortcode(shortcode, response)
