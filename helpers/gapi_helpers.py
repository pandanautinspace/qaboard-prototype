from typing import List
from google.oauth2 import id_token
from google.auth.transport import requests
from google_auth_oauthlib.flow import Flow
from google.oauth2.credentials import Credentials
from googleapiclient import discovery
from helpers import users

import os

CLIENT_ID = '573153032143-nrtmn326qeoche2ks8d0rdok5t129m72.apps.googleusercontent.com'
CLIENT_SECRET_FILE = os.path.join(os.path.dirname(__file__), 'client_secret_' + CLIENT_ID + '.json')
_CALLBACK_URI = 'https://qaboard.pythonanywhere.com/oauth2callback/'
SCOPES: List[str] = ['https://www.googleapis.com/auth/userinfo.email',
                     'https://www.googleapis.com/auth/userinfo.profile',
                     'https://www.googleapis.com/auth/drive.file',
                     'https://www.googleapis.com/auth/plus.me']


def gen_flow(**kwargs):
    """Generate a Google Auth Flow object with some preloaded information pertinent to QABoard application."""
    flow = Flow.from_client_secrets_file(
        CLIENT_SECRET_FILE,
        scopes=SCOPES, **kwargs)
    flow.redirect_uri = _CALLBACK_URI
    return flow


def get_credentials_from_code(auth_code):
    """Wrapper function to retrieve the Credentials object based on a Google Authorization Code"""
    flow = gen_flow()
    flow.fetch_token(code=auth_code)
    return flow.credentials


def get_credentials_from_auth_response(auth_response):
    """Wrapper function to retrieve the Credentials object based on a Google Auth Response"""
    flow = gen_flow()
    flow.fetch_token(authorization_response=auth_response)
    return flow.credentials


def get_refresh_token_from_auth_response(auth_response):
    """Wrapper function to retrieve the Credentials object based on a Google Auth Response.
    Refresh token should NOT be used for authorization."""
    creds = get_credentials_from_auth_response(auth_response)
    return creds.refresh_token


def refresh_tokens(refresh_token):
    """Wrapper function to refresh the Credentials based on a refresh token."""
    with open(CLIENT_SECRET_FILE) as secret_file:
        client_secret = secret_file.read()
    creds = Credentials(token=None, refresh_token=refresh_token,
                        token_uri=_CALLBACK_URI, client_id=CLIENT_ID,
                        client_secret=client_secret, scopes=SCOPES)
    creds.refresh()


def get_service_from_refresh_token(refresh_token, service_name='sheets', version='v4'):
    """
    Make a service based on refresh token.
    NOT FUNCTIONAL - refresh token should not be used for making services, as it turns out..
    :param refresh_token:
    :param service_name:
    :param version:
    :return:
    """
    with open(CLIENT_SECRET_FILE) as secret_file:
        client_secret = secret_file.read()
    creds = Credentials(token=None, refresh_token=refresh_token,
                        client_id=CLIENT_ID,
                        client_secret=client_secret, scopes=SCOPES)
    return discovery.build(service_name, version, credentials=creds)


def get_service_from_credentials(creds, service_name='sheets', version='v4'):
    """Wrapper function with some presets to generate a service object from Credentials."""
    return discovery.build(service_name, version, credentials=creds)


def get_user_from_auth_token(token):
    """Retrieve a user object based on a Google Auth Token retrieved from an auth prompt."""
    idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
    if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        raise ValueError('Wrong issuer.')
    user_id = idinfo['sub']
    user_email = idinfo['email']
    user_name = idinfo['name']
    return users.User(user_name, user_email, user_id)


def dict_from_credentials(credentials):
    """Convert a Credentials object into a dict for easier transport."""
    return {'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes}
