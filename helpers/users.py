from helpers.gapi_helpers import dict_from_credentials
from google.oauth2.credentials import Credentials
import json
import jsons


class User:
    """Base User class, used to hold information about Google Sign-In Details"""
    def __init__(self, name: str, email: str, google_id: str):
        self.name = name
        self.email = email
        self.google_id = google_id


class Teacher(User):
    """Teacher class inherits from User class, contains a few convenience values specific to teachers. """
    def __init__(self, name, email, google_id, credentials, sheet_id):
        super().__init__(name, email, google_id)
        self.credentials = credentials
        self.sheet_id = sheet_id

    @classmethod
    def from_dict(cls, dic):
        """Make an instance of Teacher from a dictionary.
         Useful for transferring from session storage or transporting as object."""
        return cls(dic['name'], dic['email'], dic['id'], Credentials(**dic['credentials']), dic['sheet_id'])

    @classmethod
    def from_json(cls, json_string):
        """Make an instance of Teacher from JSON string.
        Useful for getting teacher object when it is transferred over an HTTP request."""
        dic = json.loads(json_string)
        return cls(dic['name'], dic['email'], dic['id'], Credentials(**dic['credentials']), dic['sheet_id'])

    @classmethod
    def from_user(cls, user, credentials, sheet_id):
        """Make an instance of Teacher from User object.
        Useful when client has already signed in and needs to add additional details to become teacher."""
        return cls(user.name, user.email, user.google_id, credentials, sheet_id)

    @property
    def json_string(self):
        """Generate a JSON string representation of the Teacher object. """
        dic = {'name': self.name, 'email': self.email, 'id': self.google_id,
               'credentials': dict_from_credentials(self.credentials),
               'sheet_id': self.sheet_id}
        return json.dumps(dic)

    def get_json_string(self):
        """Generate a JSON string representation of entirety of Teacher object."""
        return jsons.dumps(self)


class Student(User):
    pass
