import markdown
import time
import random
import hashids


class Question:
    """Question class organizes the details of a question."""
    def __init__(self, ID, title, body, owner):
        self.ID = ID
        self.title = title
        self.body = body
        self.owner = owner

    def render(self):
        """Render the question's body from markdown to HTML"""
        md = markdown.Markdown(extensions=['mdx_math'])
        return md.convert(self.body)


class Response:
    """Response class organizes the details of a response."""
    def __init__(self, identifier, body, owner, tags=None):
        self.id = identifier
        self.body = body.replace('<', "&lt;").replace('>', "&gt;")
        self.owner = owner
        if tags:
            self.tags = tags
        else:
            self.tags = []

    def render(self):
        """Render the question's body from markdown to HTML"""
        md = markdown.Markdown(extensions=['mdx_math'])
        return md.convert(self.body)


class ResponseList:
    """ResponseList class holds a group of responses, contains a few wrapper functions."""
    responseCount: int

    def __init__(self, question_id):
        self.responses = []
        self.questionId = question_id
        self.responseCount = 0

    def add_response(self, response):
        self.responses.append(response)
        self.responseCount += 1

    def get_response(self, ID):
        return self.responses[ID]


class QuestionList:
    """QuestionList class holds a group of responses, contains a few wrapper functions."""
    def __init__(self):
        self.questions = {}

    def add_question(self, question):
        self.questions[question.ID] = question

    def get_question(self, ID):
        return self.questions[ID]


class QAData:
    """DEPRECATED! QAData class organizes all of the Question and Answer data.
    This is unused because it is unreasonable to store all the backend information in one
    class. Preserved for organizational purposes."""
    def __init__(self):
        self._questionList = QuestionList()
        self._responseListDict = {}

    def add_question(self, question_title, question_body, question_owner):
        h = hashids.Hashids()
        question_id = h.encode(time.time_ns())
        new_question = Question(question_id, question_title, question_body, question_owner)
        self._questionList.add_question(new_question)
        self._responseListDict[question_id] = ResponseList(question_id)

    def get_question(self, question_id):
        return self._questionList.get_question(question_id)

    def get_response_list(self, question_id):
        return self._responseListDict[question_id]

    def add_response(self, question_id, response_body, response_owner):
        h = hashids.Hashids()
        new_response = Response(h.encode(random.randint(0, 16376)), response_body, response_owner)
        self._responseListDict[question_id].add_response(new_response)

    def get_all_questions(self):
        return self._questionList.questions

    @property
    def get_question_list(self):
        return self._questionList

