from helpers import datastructures

_MAX_ATTEMPTS = 10


def _create_RowData_from_items(*args):
    """Private helper function - wraps a list of items into a RowData object to be used in a request."""
    values = []
    for item in args:
        value_type = 'errorValue'
        if isinstance(item, int) or isinstance(item, float):
            value_type = 'numberValue'
        elif isinstance(item, str):
            value_type = 'stringValue'
        values.append({
            'userEnteredValue': {
                value_type: item
            }
        })
    return {
        'values': values
    }


def _create_ValueRange_from_items(*items):
    """Private helper function - wraps a list of items into a ValueRange object to be used in a request."""
    values = []
    for item in items:
        values.append(item)
    return {
        'values': [values]
    }


def create_new_datasheet(service):
    """Creates a new Google Sheet preformatted to be used for storing Question and Answer data."""
    heading_range = 'A1:A5'
    students_heading = [{
        'startRow': 0,
        'startColumn': 0,
        'rowData': [
            _create_RowData_from_items('Student', 'Notes', 'ID')
        ]
    }]
    students_sheet = {
        'properties': {
            'title': 'Students'
        },
        'data': students_heading
    }
    questions_heading = [{
        'startRow': 0,
        'startColumn': 0,
        'rowData': [
            _create_RowData_from_items('Question Shortcode', 'Question ID', 'Question Title', 'Question Body')
        ]
    }]
    questions_sheet = {
        'properties': {
            'title': 'Questions'
        },
        'data': questions_heading
    }
    spreadsheet = {
        'properties': {
            'title': 'QA Data Storage'
        },
        'sheets': [
            students_sheet,
            questions_sheet
        ]
    }
    call = service.spreadsheets().create(body=spreadsheet, fields='spreadsheetId').execute()
    return call.get('spreadsheetId')


def add_responses_sheet(service, spreadsheet_id):
    """Add a sheet for responses preformatted to hold response data for a question."""
    batch_update_create = {
        'requests': [{
            'addSheet': {
            }
        }]
    }
    result = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id,
                                                body=batch_update_create)
    response = result.execute()
    sheet_id = response.get('replies')[0].get('addSheet').get('properties').get('sheetId')
    requests = []
    requests.append({
        'updateSheetProperties': {
            'properties': {
                'sheetId': sheet_id,
                'title': 'Responses{0}'.format(sheet_id)
            },
            'fields': 'title'
        }
    })
    requests.append({
        'appendCells': {
            'sheetId': sheet_id,
            'rows': [
                _create_RowData_from_items('Student', 'Tags', 'Student ID', 'Response ID')
            ],
            'fields': '*'
        }
    })
    batch_update_sheet_id = {
        'requests': requests
    }
    result = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id,
                                                body=batch_update_sheet_id)
    result.execute()
    return sheet_id


def add_question(service, spreadsheet_id, question):
    """Insert a question's information into the questions sheet in Google Sheets.
    """
    sheet_id = add_responses_sheet(service, spreadsheet_id)
    body = question.body
    if not body:
        body = "_Empty Body_"
    question_values = [question.ID, sheet_id, question.title, body]
    sheets = service.spreadsheets()
    body = _create_ValueRange_from_items(*question_values)
    sheets.values().append(body=body, spreadsheetId=spreadsheet_id, range='Questions!A:D',
                           valueInputOption='RAW').execute()
    return sheet_id


def get_sheet_id_from_shortcode(service, spreadsheet_id, question_shortcode):
    """Retrieve the sheet id of a question's response sheet based on its shortcode."""
    sheet_range = 'Questions!A2:B'
    request = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=sheet_range,
                                                  majorDimension='COLUMNS').execute()
    values = request.get('values')
    sheet_index = values[0].index(question_shortcode)
    return values[1][sheet_index]


def get_respondent_id(service, spreadsheet_id, respondent_name):
    """Get the id of a respondent based on their name."""
    sheet_range = 'Students!A2:A'
    request = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                  range=sheet_range,
                                                  majorDimension='COLUMNS').execute()
    values = request.get('values')

    try:
        existing_index = values[0].index(respondent_name)
        return existing_index + 1
    except ValueError:
        append_values = [respondent_name, '', len(values) + 1]
        request_body = _create_ValueRange_from_items(*append_values)
        service.spreadsheets().values().append(body=request_body, spreadsheetId=spreadsheet_id,
                                               range=sheet_range, valueInputOption='RAW').execute()
        return len(values) + 1
    except TypeError:
        append_values = [respondent_name, '', 1]
        request_body = _create_ValueRange_from_items(*append_values)
        service.spreadsheets().values().append(body=request_body, spreadsheetId=spreadsheet_id,
                                               range=sheet_range, valueInputOption='RAW').execute()
        return 1


def get_name_from_respondent_id(service, spreadsheet_id, respondent_id):
    """Get the name of a respondent from their id in the google sheet."""
    respondent_id = int(respondent_id)-1
    sheet_range = 'Students!A2:C'
    request = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                  range=sheet_range,
                                                  majorDimension='COLUMNS').execute()
    values = request.get('values')
    if values:
        return values[0][respondent_id]


def add_response_to_question(service, spreadsheet_id, question_shortcode, response):
    """Add a response to a question based on its shortcode."""
    # get sheet_id based on shortcode and spreadsheet_id
    sheet_id = get_sheet_id_from_shortcode(service, spreadsheet_id, question_shortcode)
    sheet_range = 'Responses{0}!A:D'.format(sheet_id)
    respondent_id = get_respondent_id(service, spreadsheet_id, response.owner)
    values = [response.body, ''.join(response.tags), respondent_id, response.id]
    request_body = _create_ValueRange_from_items(*values)
    service.spreadsheets().values().append(body=request_body, spreadsheetId=spreadsheet_id,
                                           range=sheet_range, valueInputOption='RAW').execute()


def get_question(service, spreadsheet_id, question_shortcode, owner):
    """Get a question's information based on its shortcode and the sheet its contained in."""
    shortcodes_range = 'Questions!A2:A'
    ids_range = 'Questions!B2:B'
    titles_range = 'Questions!C2:C'
    bodies_range = 'Questions!D2:D'
    shortcodes = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                     range=shortcodes_range,
                                                     majorDimension='COLUMNS').execute().get('values')[0]
    index_of_question = shortcodes.index(question_shortcode)
    titles = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=titles_range,
                                                 majorDimension='COLUMNS').execute().get('values')[0]
    bodies = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=bodies_range,
                                                 majorDimension='COLUMNS').execute().get('values')[0]
    ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                              range=ids_range,
                                              majorDimension='COLUMNS').execute().get('values')[0]
    title = titles[index_of_question]
    body = ""
    if index_of_question < len(bodies):
        body = bodies[index_of_question]
    question_id = shortcodes[index_of_question]
    return datastructures.Question(question_id, title, body, owner)


def get_question_by_id(service, spreadsheet_id, question_id, owner):
    """Retrieve question's information based on its id within the teacher's spreadsheet."""
    shortcodes_range = 'Questions!A2:A'
    ids_range = 'Questions!B2:B'
    titles_range = 'Questions!C2:C'
    bodies_range = 'Questions!D2:D'
    shortcodes = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                     range=shortcodes_range,
                                                     majorDimension='COLUMNS').execute().get('values')[0]
    titles = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=titles_range,
                                                 majorDimension='COLUMNS').execute().get('values')[0]
    bodies = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=bodies_range,
                                                 majorDimension='COLUMNS').execute().get('values')[0]
    ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                              range=ids_range,
                                              majorDimension='COLUMNS').execute().get('values')[0]
    index_of_question = ids.index(question_id)
    title = titles[index_of_question]
    body = ""
    if index_of_question < len(bodies):
        body = bodies[index_of_question]
    return datastructures.Question(question_id, title, body, owner)


def get_questionList(service, spreadsheet_id):
    """Retrieve list of questions in QuestionList object from a spreadsheet given by its id."""
    shortcodes_range = 'Questions!A2:A'
    ids_range = 'Questions!B2:B'
    titles_range = 'Questions!C2:C'
    bodies_range = 'Questions!D2:D'
    ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                              range=ids_range,
                                              majorDimension='COLUMNS').execute().get('values')
    shortcodes = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                     range=shortcodes_range,
                                                     majorDimension='COLUMNS').execute().get('values')
    titles = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=titles_range,
                                                 majorDimension='COLUMNS').execute().get('values')
    bodies = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=bodies_range,
                                                 majorDimension='COLUMNS').execute().get('values')
    ql = datastructures.QuestionList()
    if shortcodes:
        shortcodes = shortcodes[0]
        ids = ids[0]
        titles = titles[0]
        bodies = bodies[0]
        for i in range(len(shortcodes)):
            body = " "
            if i < len(bodies):
                body = bodies[i]
            question = datastructures.Question(shortcodes[i], titles[i], body, owner='self')
            ql.add_question(question)
    return ql


def get_last_question(service, spreadsheet_id):
    """Retrieve the last question in the storage sheet represented by the spreadsheet id."""
    shortcodes_range = 'Questions!A2:A'
    ids_range = 'Questions!B2:B'
    titles_range = 'Questions!C2:C'
    bodies_range = 'Questions!D2:D'
    ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                              range=ids_range,
                                              majorDimension='COLUMNS').execute().get('values')
    shortcodes = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                     range=shortcodes_range,
                                                     majorDimension='COLUMNS').execute().get('values')
    titles = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=titles_range,
                                                 majorDimension='COLUMNS').execute().get('values')
    bodies = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=bodies_range,
                                                 majorDimension='COLUMNS').execute().get('values')
    if titles:
        titles = titles[0]
        bodies = bodies[0]
        shortcodes = shortcodes[0]
        title = titles[-1]
        body = bodies[-1]
        question_id = shortcodes[-1]
        return datastructures.Question(ID=question_id, body=body, title=title,
                                       owner='')


def get_responseList(service, spreadsheet_id, question_shortcode) -> datastructures.ResponseList:
    """Retrieve the last question in the storage sheet represented by the spreadsheet id."""
    sheet_id = get_sheet_id_from_shortcode(service, spreadsheet_id, question_shortcode)
    responses_range = 'Responses{0}!A2:D'.format(sheet_id)
    responses = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                    range=responses_range).execute().get('values')
    result = datastructures.ResponseList(question_shortcode)
    if responses:
        for response in responses:
            response_body = response[0]
            response_tags = response[1].split(',')
            respondent_id = response[2]
            response_id = response[3]
            respondent_name = get_name_from_respondent_id(service, spreadsheet_id, respondent_id)
            response_object = datastructures.Response(body=response_body,
                                                      tags=response_tags,
                                                      identifier=response_id,
                                                      owner=respondent_name)
            result.add_response(response_object)
    return result


def add_tag(service, spreadsheet_id, question_id, response_id, tag):
    """Add a a tag to a response in the google sheet."""
    tag = tag.strip()
    sheet_id = get_sheet_id_from_shortcode(service, spreadsheet_id, question_id)
    response_ids_range = 'Responses{0}!D2:D'.format(sheet_id)
    response_ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                       range=response_ids_range,
                                                       majorDimension='COLUMNS').execute().get('values')[0]
    line = response_ids.index(response_id) + 2
    response_tags_range = 'Responses{0}!B{1}'.format(sheet_id, line)
    response_tags = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                        range=response_tags_range).execute().get('values')

    new_response_tags = response_tags[0][0] + ',' + tag if response_tags else tag
    values = [
        [new_response_tags]
    ]
    body = {
        'values': values
    }
    value_input_option = 'USER_ENTERED'
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, range=response_tags_range,
        valueInputOption=value_input_option, body=body).execute()


def get_tags(service, spreadsheet_id, question_id, response_id):
    sheet_id = get_sheet_id_from_shortcode(service, spreadsheet_id, question_id)
    response_ids_range = 'Responses{0}!D2:D'.format(sheet_id)
    response_ids = ''
    response_ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                       range=response_ids_range,
                                                       majorDimension='COLUMNS').execute().get('values')[0]
    line = response_ids.index(response_id) + 2
    response_tags_range = 'Responses{0}!B{1}'.format(sheet_id, line)

    response_tags = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                        range=response_tags_range).execute().get('values')
    if response_tags:
        return response_tags[0][0].split(',')
    else:
        return None


def set_tags(service, spreadsheet_id, question_id, response_id, tags):
    """Remove a tag from a response in the google sheet."""
    sheet_id = get_sheet_id_from_shortcode(service, spreadsheet_id, question_id)
    response_ids_range = 'Responses{0}!D2:D'.format(sheet_id)
    response_ids = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                       range=response_ids_range,
                                                       majorDimension='COLUMNS').execute().get('values')[0]
    line = response_ids.index(response_id) + 2
    response_tags_range = 'Responses{0}!B{1}'.format(sheet_id, line)
    response_tags = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                        range=response_tags_range).execute().get('values')
    new_response_tags = ''.join(tags)
    values = [
        [new_response_tags]
    ]
    body = {
        'values': values
    }
    value_input_option = 'USER_ENTERED'
    service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, range=response_tags_range,
        valueInputOption=value_input_option, body=body).execute()


def remove_tag(service, spreadsheet_id, question_id, response_id, tag):
    tags = get_tags(service, spreadsheet_id, question_id, response_id)
    tags.remove(tag.strip())
    return set_tags(service, spreadsheet_id, question_id, response_id, tags)
